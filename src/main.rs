use std::{collections::HashMap, time::Instant};

const N: usize = 20000;
const TARGET: usize = 1e18 as usize;
const MAX_PER: usize = 200;
const NUM_PER: usize = 10;

enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    pub fn cw(&self) -> Direction {
        match self {
            Direction::Up => Direction::Right,
            Direction::Right => Direction::Down,
            Direction::Down => Direction::Left,
            Direction::Left => Direction::Up,
        }
    }

    pub fn ccw(&self) -> Direction {
        match self {
            Direction::Up => Direction::Left,
            Direction::Left => Direction::Down,
            Direction::Down => Direction::Right,
            Direction::Right => Direction::Up,
        }
    }
}

struct Grid {
    pos: (i64, i64),
    dir: Direction,
    tiles: HashMap<(i64, i64), bool>,
}

impl Default for Grid {
    fn default() -> Self {
        Grid {
            pos: (0, 0),
            dir: Direction::Up,
            tiles: HashMap::new(),
        }
    }
}

impl Grid {
    pub fn step(&mut self) {
        let entry = self.tiles.entry(self.pos).or_insert(false);
        if *entry {
            self.dir = self.dir.ccw();
        } else {
            self.dir = self.dir.cw();
        }
        *entry = !*entry;
        match self.dir {
            Direction::Up => self.pos.1 += 1,
            Direction::Down => self.pos.1 -= 1,
            Direction::Right => self.pos.0 += 1,
            Direction::Left => self.pos.0 -= 1,
        }
    }

    pub fn score(&self) -> i64 {
        self.tiles.values().filter(|&x| *x).count() as i64
    }
}

fn main() {
    let start = Instant::now();
    let mut y = Vec::new();

    let mut grid = Grid::default();
    for _ in 0..N {
        grid.step();
        let s = grid.score();
        y.push(s);
    }
    let mut per = 1;
    'outer: loop {
        if per > MAX_PER {
            panic!("No period found.");
        }
        for i in 0..NUM_PER {
            for j in 0..per {
                if y[N - 1 - i * per - j] - y[N - 2 - i * per - j] != y[N - 1 - j] - y[N - 2 - j] {
                    per += 1;
                    continue 'outer;
                }
            }
        }
        break;
    }

    let cycles = ((TARGET - N) / per) as i64;
    let offset = (TARGET - N) % per;

    let res =
        y[N - 1] + cycles * (y[N - 1] - y[N - 1 - per]) + y[N - 1 - per + offset] - y[N - 1 - per];
    println!("{}", res);
    println!("{:}ms", (Instant::now() - start).as_micros() as f64 / 1000.);
}
